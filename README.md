# Her - A chatbot that answers your questions #

### What is this repository for? ###
* A nodejs based bot that answers to user questions.
* It's like Quora for bot.
* Ask bot questions and upvote answers you like the most. (You can ask bot to reply differnt answer to the questions)
* You can help bot learn by answer the questions.

### How do I get set up? ###

* Upload the code to Firsebase as cloud functions
* Setup an account on DialogFlow (API.AI)
* Use the api endpoint with DialogFlow as a webhook to connect Dialogflow and Firebase cloud function.
* For Google Assistant to enable authenticaton use "auth" and "token" api
* Refer to Google Assistant developer documentation on how to integrate authentication.