const admin = require('firebase-admin');
const dotenv = require('dotenv');
const serviceAccount = require("../config/her-ocerqg-firebase-adminsdk-fn2s5-42b70ba0a6.json");

dotenv.load();

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: process.env.FIREBASE_DATABASE_URL
});

exports.db = admin.database();