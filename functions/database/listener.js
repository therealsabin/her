const db = require('./database').db;
const algoliaIndex = require("../algolia/questions").algolia_index;
const ref = db.ref('fireblog');
const question = {

  updateInboxQuesCount(user_id) {
    const inbox = ref.child('inbox').child(user_id);
    inbox.once('value').then(snap => {
      const questions_count = snap.val().questions
      return questions_count + 1;
    }).then(count => {
      inbox.update({
        questions: count 
      });
    });
  },

  updateInboxOnAnswer(snap) {
    console.log("inboxAns", JSON.stringify(snap.val()));
    snap.forEach((child) => {
      const inbox = ref.child('inbox').child(child.val().user_id);
      const owner = ref.child('inbox').child(child.val().owner);
      
      inbox.once('value').then(snap => {
        const answers_count = snap.val().answers;
        return answers_count + 1;
      }).then(count => {
        return inbox.update({
          answers: count 
        });
      }).then(() => {
        return owner.once('value');
      }).then(snap => {
        const responses_count = snap.val().responses;
        return responses_count + 1;
      }).then(count => {
        owner.update({
          responses: count
        });
      });
    });
  },

  increaseQuestionCount(dataSnapshot) {
    let questionCount = dataSnapshot.val().id;
    console.log("listener question count:::: ", questionCount);
    ref.child('data').set({
      questions: questionCount
    }).then(() => {
      this.updateInboxQuesCount(dataSnapshot.val().user_id);
    });

    algoliaIndex.addIndexRecord(dataSnapshot);
  },


}

const questionsRef = ref.child("questions");
questionsRef.on('child_added', question.increaseQuestionCount.bind(question));

const answersRef = ref.child("answers");
answersRef.on('child_added', question.updateInboxOnAnswer);
// contactsRef.on('child_changed', addOrUpdateIndexRecord);
// contactsRef.on('child_removed', deleteIndexRecord);