const db = require('./database').db;

const ref = db.ref('fireblog');

const user = {
  getCount: function () {
    return ref.child("data").once('value').then(function(snapshot) {
      return snapshot.val().questions;
    });
  },

  getRandomInt: function (min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
  },

  getUser: function (user_id) {
    let userRef = ref.child('users');
    return userRef.orderByKey().equalTo(user_id).once('value').then((snapshot) => {
      return snapshot.val();
    }, (err) => {
      console.log("database/utils.js", err); 
    }).then(snapVal => {
      return ref.child("inbox").child(user_id).once('value').then(snap => {
          const value = snap.val();
          console.log("INbox Welcome", JSON.stringify(value));
          snapVal[user_id]['answers'] = value.answers;
          snapVal[user_id]['questions'] = value.questions;
          snapVal[user_id]['responses'] = value.responses;
          snapVal[user_id]['id'] = user_id;
        console.log("Inbox SnapData", snapVal[user_id]);
        return snapVal[user_id];
      });
    });
  },

  addQuestion: function (user_id, question) {
    return ref.child("data").once('value').then(function(snapshot) {
      console.log("question count:::: ", snapshot.val().questions);
      let questionCount = snapshot.val().questions + 1;
      let postRef = ref.child('questions');
      return postRef.push({
          id: questionCount,
          title: question,
          user_id: user_id
        });
    });
  },

  addAnswer: function (user_id, ques_key, answer, ques_user_id) {
    let postRef = ref.child('answers').child(ques_key);
    let mapRef = ref.child('map').child(ques_key);
    return postRef.push({
      desc: answer,
      user_id: user_id,
      owner: ques_user_id,
      read_status: false,
      rating: 0
    });
    // }).then((ref) => {
    //     let value = {};
    //     value[ref.key] = true;
    //     return mapRef.push(value);      
    // });
  },

  toggleAnswerRating: function (user_id, answer_key, ques_key) {
    return ref.child("answers")
      .child(ques_key)
      .child(answer_key)
      .once('value')
      .then(snap => {
        if (snap.hasChildren()) {
          return run()
        } else {
          return {
            didCommit: false,
            like: null,
            invalid_key: true
          };
        }
      });

    function toggle(like = true) {
      var toggleRef = ref.child('answers').child(ques_key).child(answer_key);
      return toggleRef.transaction(function(answer) {
        if (answer) {
          if (like) {
            answer.rating++;
          } else {
            answer.rating--;
          }
        }
        return answer;
      }).then((data) => {
        return {
          didCommit: data.committed,
          like: like,
          invalid_key: false
        };
      });
    }
  
    function run() {
      ratingRef = ref.child("rating").child(answer_key);

      return ratingRef
      .orderByKey()
      .equalTo(user_id)
      .once('value')
      .then((snapshot) => {
        //console.log("SnapNumChild", snapshot.numChildren());
        //console.log("SnapHasChild", snapshot.hasChildren());
        //console.log("SnapHasChild", snapshot.hasChild("user_id"));
        if (snapshot.hasChildren()) {
          const ratingUser = {};
          ratingUser[user_id] = null;
          return ratingRef.update(ratingUser).then(() => {
            return toggle(false);
          });
        } else {
          const ratingUser = {};
          ratingUser[user_id] = true;
          return ratingRef.update(ratingUser).then(toggle);
        }
      });
    }
  },

  generateAnswerKey: function (isNext, key) {
    if (isNext) {
        var charCode = key.charCodeAt(19) - 1;
    } else {
        var  charCode = key.charCodeAt(19) + 1;
    }
    const char = String.fromCharCode(charCode);
    return key.substr(0, 19) + char;
  },

  getAnswer: function (objectID, isFirst = true, isNext, value, key) {
    if (!isFirst){
      let query = ref.child('answers')
        .child(objectID)
        .orderByChild("rating");

      if (isNext) {
        query = query.endAt(value, key).limitToLast(1);  
      } else {
        query = query.startAt(value, key).limitToFirst(1)
      }
      
      return query.once('value').then((snapshot) => {
        let answer;
        snapshot.forEach(function(child) {
          answer = child;
        });
        return answer;
      });
    }else{
      return ref.child('answers').child(objectID)
      .orderByChild("rating")
      .limitToLast(1)
      .once('value')
      .then((snapshot) => {
        let answer;
        snapshot.forEach(function(child) {
          answer = child;
        });
        return answer;
      });
    }
  },

  getAnswerMap: function(objectID) {
    return ref.child('map')
      .orderByKey()
      .equalTo(objectID)
      .once('value')
      .then((snapshot) => {
        let result = [];
        snapshot.forEach((child) => {
          console.log(`This is the child we've been talking about: ${JSON.stringify(child.val())}`);
          const innerChild = child.val();
          for (var key in innerChild) {
            const answerObj = innerChild[key];
            for (var answerKey in answerObj){
              result.push(answerKey);
            }
          }
        });
        return result;
      });
  },

  getRandomQuestion: function() {
    return this.getCount().then((count) => {
      const randomInt = this.getRandomInt(1, count + 1);
      return ref.child('questions')
        .equalTo(randomInt)
        .orderByChild('id')
        .once('value')
        .then((snapshot) => {
          let data;
          snapshot.forEach(function(child) {
            console.log(child.key+": "+child.val());
            //data = child.val();
            data = child;
            //console.log("MySnappyData::inside:", data.title);
          });
          //console.log("MySnappyData", data.title);
          return data;
        });
    });
  },
}

exports.user = user;