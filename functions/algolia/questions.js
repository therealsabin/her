const db = require('../database/database').db;
const algolia = require('./config');
const {ALGOLIA_QUESTION_INDEX_NAME} = require('./constants');

const question = {
  addIndexRecord(dataSnapshot) {
    const index = algolia.initIndex(ALGOLIA_QUESTION_INDEX_NAME);
    // Get Firebase object
    const firebaseObject = dataSnapshot.val();
    // Specify Algolia's objectID using the Firebase object key
    firebaseObject.objectID = dataSnapshot.key;
    // Add or update object
    index.saveObject(firebaseObject, function(err, content) {
      if (err) {
        throw err;
      }
      console.log('Firebase object indexed in Algolia', firebaseObject.objectID);
    });
  }
}

exports.algolia_index = question
//const questionsRef = db.ref("fireblog/questions");
//questionsRef.on('child_added', question.addIndexRecord);
// contactsRef.on('child_changed', addOrUpdateIndexRecord);
// contactsRef.on('child_removed', deleteIndexRecord);