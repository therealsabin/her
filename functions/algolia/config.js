const dotenv = require('dotenv');
const algoliasearch = require('algoliasearch');

dotenv.load();

// configure algolia
const algolia = algoliasearch(process.env.ALGOLIA_APP_ID, process.env.ALGOLIA_API_KEY);
//module.exports = algolia.initIndex('questions');
module.exports = algolia;