const algolia = require('./config');
const {ALGOLIA_QUESTION_INDEX_NAME} = require('./constants');

const search = {
  query: (query) => {
    const index = algolia.initIndex(ALGOLIA_QUESTION_INDEX_NAME);
    return index.search(query).then(content => {
      //console.log(`Search result for : (${query}) : `, content); 
      return content; 
    });
  }
}

module.exports = search;