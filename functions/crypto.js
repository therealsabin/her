const crypto = require('crypto');
var aesjs = require('aes-js');
//var secret = crypto.randomBytes(24);
const secret = "0123456789abcdef";
const iv = "0123456789abcdef";

// var key = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 ];
 
// // The initialization vector (must be 16 bytes) 
// var iv = [ 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34,35, 36 ];
 


exports.encrypt = (plaintext) => {
    var cipher = crypto.createCipher('aes-128-cbc', secret);
    cipher.setAutoPadding(false);
    // var ciphertext = '';
    // for (var i=0; i < plaintext.length; i+=16) {
    //     ciphertext += cipher.update(plaintext.substr(i, i+16), 'utf8', 'base64');
    // }
    ciphertext = cipher.update(plaintext, 'utf8', 'base64');
    //ciphertext += cipher.final('base64');
    return ciphertext.toString('base64');
}

exports.decrypt = (ciphertext) => {
    var decipher = crypto.createDecipher('aes-128-cbc', secret);
    decipher.setAutoPadding(false);
    var plaintext = decipher.update(ciphertext, 'base64', 'utf8');
    return plaintext.toString('utf8');
}

// exports.encrypt = (text) => {
// 	var textBytes = aesjs.utils.utf8.toBytes(text);	
// 	var aesCbc = new aesjs.ModeOfOperation.cbc(key, iv);
// 	var encryptedBytes = aesCbc.encrypt(textBytes);
	
// 	// To print or store the binary data, you may convert it to hex 
// 	var encryptedHex = aesjs.utils.hex.fromBytes(encryptedBytes);
// 	return encryptedHex;
// }

// exports.decrypt = (ciphertext) => {
//    // When ready to decrypt the hex string, convert it back to bytes 
// 	var encryptedBytes = aesjs.utils.hex.toBytes(ciphertext);
	
// 	// The cipher-block chaining mode of operation maintains internal 
// 	// state, so to decrypt a new instance must be instantiated. 
// 	var aesCbc = new aesjs.ModeOfOperation.cbc(key, iv);
// 	var decryptedBytes = aesCbc.decrypt(encryptedBytes);
	
// 	// Convert our bytes back into text 
// 	var decryptedText = aesjs.utils.utf8.fromBytes(decryptedBytes);
// 	return decryptedText;
// }