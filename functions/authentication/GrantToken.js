'use strict'
const jwt = require('jsonwebtoken');

function GrantToken(body, decoder, secret, encryption, ALGORITHM_TYPE) {
  const TOKEN_EXPIRE_TIME = 60000; //3600000; // 1 Hour

  this.body = body;
  this.decoder = decoder;
  this.secret = secret;
  this.encryption = encryption;
  this.ALGORITHM_TYPE = ALGORITHM_TYPE;

  const getAccessToken = (user_id, client_id) => {
    return {
      user_id: user_id,
      client_id: client_id,
      type: "ACCESS",
      expires_at: Date.now()+ TOKEN_EXPIRE_TIME
    };
  }

  const getRefreshToken = (user_id, client_id) => {
    return {
      user_id: user_id,
      client_id: client_id,
      type: "REFRESH"
    }
  }

  const jwtBearer = () => {
    const {assertion, intent, scope, client_id, client_secret} = body;
    /** To do verification

    **/
    // To do verify authenticity of token
    const token = jwt.decode(assertion);
    const {sub, aud} = token;

    const ACCESS_TOKEN = getAccessToken(sub, aud);
    const REFRESH_TOKEN = getRefreshToken(sub, aud);

    const result = {
      token_type: "bearer",
      access_token: "",
      refresh_token: "",
      expires_in: TOKEN_EXPIRE_TIME / 1000
    };

    return encryption.getSameKeyAndIV(secret).then((data) => {
      const enc_access_token = encryption.encryptText(
        ALGORITHM_TYPE, data.key, 
        data.iv, new Buffer(JSON.stringify(ACCESS_TOKEN)).toString('utf8'), "base64"
      );
      const enc_refresh_token = encryption.encryptText(
        ALGORITHM_TYPE, data.key, 
        data.iv, new Buffer(JSON.stringify(REFRESH_TOKEN)).toString('utf8'), "base64"
      );
    
      // console.log("data.key", data.key.toString('utf8'));
      // console.log("data.iv", data.iv.toString('base64'));

      result.access_token = enc_access_token;
      result.refresh_token = enc_refresh_token;
      return {
        response: result,
        token: token
      };
    }, (error) => {
      console.log(error);
    });
  }

  const authorization = () => {
    const {grant_type, code, redirect_uri, client_id, client_secret} = body;
    console.log("Authorization", "Access Token called");
  }

  const refresh = () => {
    const {grant_type, client_id, client_secret, refresh_token} = body;
    console.log('RefreshBody', body);
    const result = {
      token_type: "bearer",
      access_token: "",
      expires_in: 3600
    };

    return decoder(refresh_token).then((data) => {
      return {user_id: data.user_id, client_id: data.client_id};
    },(err) => {
      console.log(err);
    }).then((data) => {
      const ACCESS_TOKEN = getAccessToken(data.user_id, data.client_id);
    /**
     * To-do: Need to verify Refresh Token, client_id, user_id
     */
      return encryption.getSameKeyAndIV(secret).then((data) => {
        const enc_access_token = encryption.encryptText(
          ALGORITHM_TYPE, data.key, 
          data.iv, new Buffer(JSON.stringify(ACCESS_TOKEN)).toString('utf8'), "base64"
        );

        result.access_token = enc_access_token;
        console.log("Granttype-refresh", result);
        return result;
      }, (error) => {
        console.log(error);
      });
    });
  }

  return {
    authorization: authorization,
    refresh: refresh,
    jwtBearer: jwtBearer,
  }
}

exports.GrantToken = GrantToken;