"use strict";
//source https://gist.github.com/yoavniran/c78a0991e0152b306c25
const crypto = require("crypto");

const EncryptionHelper = (() => {
    
  const getKeyAndIV = (key, callback) => {
    crypto.pseudoRandomBytes(16, (err, ivBuffer) => {
      const keyBuffer  = (key instanceof Buffer) ? key : new Buffer(key) ;
      callback({
          iv: ivBuffer,
          key: keyBuffer
      });
    });
  }

  const getSameKeyAndIV = (key) => {
    return new Promise(function(resolve, reject) {
      const keyBuffer = new Buffer(key);
      const ivBuffer = new Buffer("onZY7ubaUDWtpPukiIQSvQ==", 'base64');
      if (keyBuffer != undefined && ivBuffer != undefined) {
        resolve({
          iv: ivBuffer,
          key: keyBuffer
        });
      } else {
        reject("Generated key is undefined");
      }
    });
  }

  const encryptText = (cipher_alg, key, iv, text, encoding)  => {
    const cipher = crypto.createCipheriv(cipher_alg, key, iv);
    encoding = encoding || "binary";
    let result = cipher.update(text, "utf8", encoding);
    result += cipher.final(encoding);
    return result;
  }

  const decryptText = (cipher_alg, key, iv, text, encoding) => {
    const decipher = crypto.createDecipheriv(cipher_alg, key, iv);
    encoding = encoding || "binary";
    let result = decipher.update(text, encoding);
    result += decipher.final();
    return result;
  }

  return {
    CIPHERS: {
      "AES_128": "aes128",          //requires 16 byte key
      "AES_128_CBC": "aes-128-cbc", //requires 16 byte key
      "AES_192": "aes192",          //requires 24 byte key
      "AES_256": "aes256"           //requires 32 byte key
    },
    getSameKeyAndIV: getSameKeyAndIV,
    getKeyAndIV: getKeyAndIV,
    encryptText: encryptText,
    decryptText: decryptText
  };
})();

exports.encryption = EncryptionHelper;