'use strict'
const functions = require('firebase-functions');
const { URL, URLSearchParams } = require('url');

const {encryption} = require('./encryption');
const db = require('../database/database').db;
const ref = db.ref('fireblog');
const GrantToken = require('./GrantToken').GrantToken;

//const secret = "1234567890abcdefghijklmnopqrstuv"; //32bit for AES_256
const secret = "66707FBF02A80B1E";
const AUTH_EXPIRE_TIME = 600000;
const ALGORITHM_TYPE = encryption.CIPHERS.AES_128_CBC;


exports.auth = functions.https.onRequest((request, response) => {
  const {response_type, client_id, redirect_uri, state} = request.query;
  const redirectURL = new URL(redirect_uri);
  //To-do: Implement OAuth 2.0 authentication: Validate client_id

  const AUTHORIZATION_TOKEN = {
    user_id: "random_id",// need to implement this by making the user login
    client_id: client_id,
    type: "AUTH_CODE",
    expires_at: Date.now()+ AUTH_EXPIRE_TIME
  };
  
  encryption.getSameKeyAndIV(secret).then((data) => {
    const authorization_token = encryption.encryptText(
      ALGORITHM_TYPE, data.key,
      data.iv, new Buffer(JSON.stringify(AUTHORIZATION_TOKEN)).toString('utf8'), "base64"
    );

    const params = new URLSearchParams({
      code: authorization_token,
      state: state
    });

    redirectURL.search = params;
    console.log("AUTH_REDIRECT_URL", redirectURL.href);
    response.redirect(redirectURL.href);
  }, (err) => {
    console.log(err);
  });
});

const decode = (token) => {
  return new Promise((resolve, reject) => {
    encryption.getSameKeyAndIV(secret).then((data) => { //using 16 byte key
      const decrypt_token = encryption.decryptText(
        ALGORITHM_TYPE, data.key, 
        data.iv, new Buffer(token).toString('utf8'), "base64"
      );
      if (decrypt_token) {
        resolve(JSON.parse(decrypt_token));
      }else{
        reject("Token invalid");
      }
    }, (err) => {
      reject(err);
    });
  });
};

exports.token = functions.https.onRequest((request, response) => {
  const body = request.body;
  const grantToken = new GrantToken(body, decode, secret, encryption, ALGORITHM_TYPE);
  let result;
  switch (body.grant_type) {
    case "urn:ietf:params:oauth:grant-type:jwt-bearer":
      grantToken.jwtBearer().then((data) => {
        const {name, email, picture, locale, sub} = data.token;
        const userRef = ref.child(`users/${sub}`);
        userRef.set({
          name: name,
          email: email,
          picture: picture,
          locale: locale
        });
        response.send(data.response);
      });
      break;
    case "authorization_code":
      result = grantToken.authorization();
      response.send(result);
      break;   
    case "refresh_token":
      grantToken.refresh().then((data) => {
        response.send(data);
      });
      break;
    default:
      response.send({
        msg: "Server could not understand you request"
      });
  }
});

exports.decode = decode;