const {user} = require('../../database/utils');

const ActionNext = (app) => {
  const canAnswer = false;
  const nextAnswer = [
    "No more answers",
    "I am out of answers",
    "That's everything I know!"
  ];

  getRandomInt = (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
  }

  const response = () => {
    const answer_rating = app.data.answer_rating;
    const ques_key = app.data.answer_ques_key;

    const answer_key = user.generateAnswerKey(true, app.data.answer_key);

    return user.getAnswer(ques_key, false, true, answer_rating, answer_key).then((child) => {
      if (child === undefined){
        app.data.answer_key = answer_key;
        return nextAnswer[getRandomInt(0, nextAnswer.length)];
      }
      app.data.answer_rating = child.val().rating;
      app.data.answer_key = child.key;
      return child.val().desc;
    });
    // if (canAnswer) {
    //   return "Your question is really good! So good that you left me speech.";      
    // } else {
    //   return nextAnswer[getRandomInt(0, nextAnswer.length)];
    // }
  }

  response().then(result => {
    app.ask(result);
  });
}

module.exports = ActionNext;