const {user} = require('../../database/utils');

const ActionRate = (app) => {
  const canAnswer = false;
  const nextAnswer = [
    "No more answers",
    "I am out of answers",
    "That's everything I know!"
  ];

  getRandomInt = (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
  }

  const response = () => {
    const ques_key = app.data.answer_ques_key;
    const user_id = app.data.user.id;
    const answer_key = app.data.answer_key;

    console.log("Rating realm *************************");
    console.log("r ques_key", ques_key);
    console.log("r user_id", user_id);
    console.log("r answer_key", answer_key);
    return user.toggleAnswerRating(user_id, answer_key, ques_key);
  }

  response().then(data => {
    if (data.didCommit && data.like) {
      app.ask("You upvoted the answer")
      console.log("UPvoted rating");
    } else if (data.didCommit && !data.like){
      app.ask("You downvoted the answer");
      console.log("DOWNvoted rating");
    } else if (data.invalid_key) {
      app.ask("You cannot upvote this response");
    } else {
      app.ask("Something went wrong while trying to rate the answer");
      console.log("Something went wrong while rating the answer");
    }
  });
}

module.exports = ActionRate;