const {decode} = require('../../authentication/oauth')
const {user} = require('../../database/utils');

const ActionOK = (app) => {
  const canAnswer = false;
  const nextAnswer = [
    "I am posting the question right away!"
  ];

  getRandomInt = (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
  }

  const response = () => {
    const question =  app.data.question;
    console.log("ActionOkUser", app.data.user);
    return user.addQuestion(app.data.user.id, question);
    //const token = app.getUser().access_token;
    // decode(token).then((data) => {
    //   const user_id = data["user_id"];
    //   user.addQuestion(user_id, question);
    // }, (error) => {
    //   return "Unable to decode the data";
    // });

    // if (canAnswer) {
    //   return "Your question is really good! So good that you left me speech.";      
    // } else {
    //   return nextAnswer[getRandomInt(0, nextAnswer.length)];
    // }
  }
  response().then(() => {
    app.ask("Your question has been added!")
  });
}

module.exports = ActionOK;