const search = require("../../algolia/search");
const {user} = require('../../database/utils');

const Question = (app) => {
  const canAnswer = false;
  const noAswerReply = [
    "I don't have answer to that question right away but can get the answer for you! Do you want to?",
    "I could ask people around the world to get the answer! Shall I?",
    "I dont have the answer to your question but I am a learner not a quitter! Shall I learn and get back to you with the answer?",
    "The answer flew right above my head LOL! But I can find it for you. Do you want to?.",
    "I am growing my knowledge day by day. Do you want me to help learn the answer to your question?"
  ];

  getRandomInt = (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
  }

  const response = () => {
    const query = app.getRawInput();
    app.data.question = query;

    return search.query(query).then(content => {
      const result = content["hits"];
      console.log(`Aloglia result hits: ${JSON.stringify(result)}`);
      if(result.length > 0) {
        let objectID = result[0].objectID;
        app.data.answer_ques_key = objectID;
        //return user.getAnswer(objectID);
        //return user.getAnswerMap(objectID).then((answerList) => {
          //console.log(`AnswerList Array:: ${JSON.stringify(answerList)}`);
          return user.getAnswer(objectID).then((child) => {
            if (child === undefined) {
              return noAswerReply[getRandomInt(0, noAswerReply.length)];      
            }
            app.data.answer_rating = child.val().rating;
            app.data.answer_key = child.key;
            return child.val().desc;
          });
        //});
        
      } else {
        return noAswerReply[getRandomInt(0, noAswerReply.length)];
      }
    });
  }

  response().then(result => {
    app.ask(result);
  });
}

module.exports = Question;