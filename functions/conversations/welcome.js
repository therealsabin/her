const {decode} = require('../authentication/oauth')
const {user} = require('../database/utils');

const Welcome = (app) => {

  const response = () => {
    const token = app.getUser().access_token;
    return decode(token).then((data) => {
      const user_id = data["user_id"];
      return user.getUser(user_id).then((data) => {
        app.data.user = data;
        return `Hey ${data.name}! You have a total of ${data.questions} questions, ${data.answers} answers and ${data.responses} responses.`;      
      });
    }, (error) => {
      return "Unable to decode the data";
    });
  }

  response().then((data) => {
    app.ask(data);
  });
}

module.exports = Welcome;