const {user} = require('../../database/utils');

const AnswerFollowup = (app) => {
  const canAnswer = false;

  const response = () => {
    const query = app.getRawInput();
    const ques_key = app.data.ques_key;
    const user_id = app.data.user.id;
    const ques_user_id = app.data.ques_user_id;
    //app.data.question = query;

    return user.addAnswer(user_id, ques_key, query, ques_user_id);
  }

  response().then(() => {
    app.ask("Your answer has been successfully recorded.");
  });
}

module.exports = AnswerFollowup;