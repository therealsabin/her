const {user} = require('../../database/utils');

const Answer = (app) => {
  const canAnswer = false;
  const noAswerReply = [
    "I don't have answer to that question right away but can get the answer for you! Do you want to?",
    "I could ask people around the world to get the answer! Shall I?",
    "I dont have the answer to your question but I am a learner not a quitter! Shall I learn and get back to you with the answer?",
    "The answer flew right above my head LOL! But I can find it for you. Do you want to?.",
    "I am growing my knowledge day by day. Do you want me to help learn the answer to your question?"
  ];

  const response = () => {
    const query = app.getRawInput();
    //app.data.question = query;

    return user.getRandomQuestion().then(data => {
      app.data.ques_key = data.key;
      app.data.ques_user_id = data.val().user_id;
      return data.val().title;
    });
  }

  response().then(result => {
    app.ask(result);
  });
}

module.exports = Answer;