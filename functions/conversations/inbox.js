const Inbox = (app) => {
  const response = () => {
    return "You've got mail!";      
  }

  app.ask(response());
}

module.exports = Inbox;