'use strict'
const functions = require('firebase-functions');
const admin = require('firebase-admin');
const ApiAiApp = require('actions-on-google').ApiAiApp;

const db = require('./database/database').db;
const ref = db.ref('fireblog');
require('./algolia/questions');
require('./database/listener');

const {auth, token, decode} = require('./authentication/oauth')
exports.auth = auth;
exports.token = token;
//exports.decode = decode;

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
 exports.helloWorld = functions.https.onRequest((request, response) => {
  let userRef = ref.child("users");
  userRef.set({
    alanisawesome: {
      data_of_birth: "June 23, 1992",
      full_name: "Alan Turing",
    },
    gracehop: {
      data_of_birth: "April 30, 1995",
      full_name: "Grace Hopper",
    }
  });
  console.log("Bot", require("./conversations/welcome"));
  response.send("Hello from Firebase!");
 });

 exports.posts = functions.https.onRequest((request, response) => {
  let postRef = ref.child('posts').child("gracehop");
  let pushPostRef = postRef.push();

  pushPostRef.set({
    author: "gracehop",
    title: "The Shunning Machine",
    height: 5
  });
  response.send("Posts from Firebase!");
 });

 exports.viewposts = functions.https.onRequest((request, response) => {
  let postRef = ref.child('posts/gracehop');
  postRef.orderByChild("height").once("value", function(snapshot){
    let result = [];
    snapshot.forEach(function(data){
      result.push(data.val().title);
    });
    response.send(result);
    // console.log(snapshot.val());
  }, function(errorObject){
    response.send("The read failed: ", errorObject.code);
    // console.log("The read failed: ", errorObject.code);
  });
  
 });

exports.foodOrder = (req, res) => {
  const app =new ApiAiApp({request: req, response: res});

  const REQUEST_PERMISSION_ACTION = 'request_permission';
   
  function requestPermission(app) {
    const permission = app.SupportedPermissions.NAME;
    app.data.permission = permission;
    app.data.drink = app.getArgument("drink");
    app.askForPermission('To know who you are', permission);
  }

  function requestSignIn(app) {

    console.log("requestsignIn", "helloooooooooooooooo");
    app.askForSignIn();
  }

  function saveUserOrder(app, drink){
    let intent = app.getIntent(); //action
    let userInput = app.getRawInput();
    //let orderEntity = app.getArgument("drink"); // Name of the entity
    let userData = app.getUserName().displayName;
    //let entity = app.getArgument("Abstract"); // Name of the entity
    //console.log("AppIntent Test", entity);
    // console.log("MyTestApp UserData", userData);
    // console.log("MyTestApp Drink", drink);
    let orderRef = ref.child('orders');
    orderRef.push({
      drink: drink,
      user: userData
    });

    let token = app.getUser().access_token;

    decode(token).then(function(body){
      console.log("MyBody", body.type);
      app.ask(`Hey ${token} Your ${drink} with userid ${app.getUser().userId} coming right up! Happy Drinking - ${body.type}`);
    }, function(err) {
          console.log(err);
      });
  }

  const SAVEORDER_INTENT = 'save.order.intent';

  function saveOrderIntent(app) {
    //  if (app.getUserName() == null){
    //  	//console.log("Display name: is null");
    // 	requestPermission(app);
    //  }

    //console.log("historymistory", req);

    if (app.isPermissionGranted()){
      let permission = app.data.permission;
      let drink = app.data.drink;
      if (permission === app.SupportedPermissions.NAME) {
        saveUserOrder(app, drink)
      }
      // let userData = app.getUserName().displayName;
      // app.tell(`Hey ${userData}, your order has been placed`);
    } else {
      app.ask("Sorry, I could not get your name hence your request could not be taken");
    }
  }

  function signIn (app) {
    console.log("signInStatus", app.getSignInStatus());
    if (app.getSignInStatus() === app.getSignInStatus.OK){
      let accessToken = app.getUser().accessToken;
      app.ask("Great, thanks for signin in!");
    }else {
      app.ask("I wont't be able to save your data, but let's continue!");
    }
  }

  const actionMap = new Map();
  actionMap.set(REQUEST_PERMISSION_ACTION, saveOrderIntent);
  actionMap.set("order.order-drink", requestPermission);
  actionMap.set("input.welcome", welcome);
  app.handleRequest(actionMap);
}

const INTENT_WELCOME = "input.welcome";
const INTENT_READ_INBOX = "input.inbox";
const INTENT_FALLBACK_ASK_QUESTION = "AskQuestion.AskQuestion-followup";
const INTENT_ASK_QUESTION_ACTIONS_NEXT = "AskQuestion-followup-next";
const INTENT_ASK_QUESTION_ACTIONS_PREV = "AskQuestion-followup-previous";
const INTENT_ASK_QUESTION_ACTIONS_OK = "AskQuestion-followup-ok";
const INTENT_ASK_QUESTION_ACTIONS_RATING = "AskQuestion-followup-rating";

const INTENT_FALLBACK_DEFAULT = "input.default_fallback";
const INTENT_HEAR_INBOX = "input.hear_inbox";

const INTENT_ANSWER_QUESTION_ACTION = "input.AnswerQuestion";
const INTENT_ANSWER_QUESTION_FOLLOWUP = "AnswerQuestion.AnswerQuestion-fallback"


exports.api = (req, res) => {  
  const app =new ApiAiApp({request: req, response: res});
  const actionMap = new Map();

  actionMap.set(INTENT_WELCOME, require("./conversations/welcome"));
  actionMap.set(INTENT_READ_INBOX, require("./conversations/inbox"));

  actionMap.set(INTENT_FALLBACK_ASK_QUESTION, require("./conversations/questions/Question"));
  actionMap.set(INTENT_FALLBACK_DEFAULT, require("./conversations/questions/Question"));
  actionMap.set(INTENT_ASK_QUESTION_ACTIONS_NEXT, require("./conversations/questions/ActionNext"));
  actionMap.set(INTENT_ASK_QUESTION_ACTIONS_PREV, require("./conversations/questions/ActionPrev"));
  actionMap.set(INTENT_ASK_QUESTION_ACTIONS_OK, require("./conversations/questions/ActionOK"));
  actionMap.set(INTENT_ASK_QUESTION_ACTIONS_RATING, require("./conversations/questions/ActionRate"))

  actionMap.set(INTENT_ANSWER_QUESTION_ACTION, require("./conversations/answers/Answer"));
  actionMap.set(INTENT_ANSWER_QUESTION_FOLLOWUP, require("./conversations/answers/AnswerFollowup"))

  // actionMap.set(INTENT_HEAR_INBOX, require(""));
  app.handleRequest(actionMap);
}
 
exports.testApi = () => {
  const testUpdate = require("./conversations/questions/ActionRate");
  testUpdate();
}
